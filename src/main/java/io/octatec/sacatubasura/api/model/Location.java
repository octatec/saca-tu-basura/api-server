package io.octatec.sacatubasura.api.model;

import io.octatec.sacatubasura.api.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Location extends DateAudit {
    @Id
    private Long id;
    @ManyToOne
    private Route route;
    private double latitude;
    private double longitude;
    private Double accuracy;
    @OneToOne
    private Truck truck;

    protected Location() {
    }


    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }


    public Double getAccuracy() {
        return accuracy;
    }
}