package io.octatec.sacatubasura.api.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Driver extends Person {
    @OneToMany(mappedBy = "driver")
    private List<DrivenRoute> drivenRoutes;
}
