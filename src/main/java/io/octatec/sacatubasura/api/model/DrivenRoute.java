package io.octatec.sacatubasura.api.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class DrivenRoute extends Route {
    @ManyToOne
    private Driver driver;
    @ManyToOne
    private Truck truck;
    @ManyToOne
    private PlannedRoute plannedRoute;
}
