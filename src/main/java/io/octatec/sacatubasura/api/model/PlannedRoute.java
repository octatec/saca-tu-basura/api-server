package io.octatec.sacatubasura.api.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class PlannedRoute extends Route {
    @OneToMany(mappedBy = "plannedRoute")
    private List<DrivenRoute> drivenRoutes;
    @ManyToOne
    private Schedule schedule;
}
