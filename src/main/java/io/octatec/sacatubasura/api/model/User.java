package io.octatec.sacatubasura.api.model;

import io.octatec.sacatubasura.api.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class User extends DateAudit {
    @Id
    private Long id;
    private String name;
    @ManyToOne
    private UserRole userRole;
    private String password;
    @OneToOne
    private Person person;
}
