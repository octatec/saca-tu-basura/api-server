package io.octatec.sacatubasura.api.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
public class UserRole {
    @Id
    private Long id;
    @OneToMany
    private List<User> users;
    private String name;
}
