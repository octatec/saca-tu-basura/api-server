package io.octatec.sacatubasura.api.model;

import io.octatec.sacatubasura.api.model.audit.DateAudit;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Route  extends DateAudit {
    @Id
    private Long id;
    @OneToMany( mappedBy = "route")
    private List<Location> locations;
}
