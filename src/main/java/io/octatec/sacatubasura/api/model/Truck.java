package io.octatec.sacatubasura.api.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Truck {
    @Id
    private Long id;
    private String plate;
    @OneToMany(mappedBy = "truck")
    private List<DrivenRoute> drivenRoutes;
    @OneToOne(mappedBy = "truck")
    private Location location;
}
