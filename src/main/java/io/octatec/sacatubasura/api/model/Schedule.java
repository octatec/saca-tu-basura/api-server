package io.octatec.sacatubasura.api.model;

import io.octatec.sacatubasura.api.model.audit.DateAudit;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Schedule extends DateAudit {
    @Id
    private Long id;
    @OneToMany(mappedBy = "schedule")
    private List<PlannedRoute> plannedRoutes;
}
