package io.octatec.sacatubasura.api.model;

import io.octatec.sacatubasura.api.model.audit.DateAudit;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Person extends DateAudit {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    @OneToOne(mappedBy = "person")
    private User user;

}
