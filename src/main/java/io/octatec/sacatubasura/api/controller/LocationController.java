package io.octatec.sacatubasura.api.controller;

import io.octatec.sacatubasura.api.model.Truck;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class LocationController {
    @MessageMapping("/location.register")
    @SendTo("/topic/public")
    public Truck register(@Payload Truck chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", Math.random());
        return chatMessage;
    }

    @MessageMapping("/location.send")
    @SendTo("/topic/public")
    public Truck sendMessage(@Payload Truck chatMessage) {
        return chatMessage;
    }
}
